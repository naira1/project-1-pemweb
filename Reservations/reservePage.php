<?php 
session_start();
if(!$_SESSION['email']) {
 return header('Location:http://localhost/project/customerSide/CustomerAccount/login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


    <title>Customer Reservation </title>
    <style>
        /* Apply background image to the body */
        body {
            font-family: 'Montserrat', sans-serif;
            background-color: rgb(37, 42, 52);
            display: flex;
            color: white;
            justify-content: center;
            /* Center the container horizontally */
            align-items: center;
            /* Center the container vertically */
            height: 100vh;
            /* Ensure the container takes up the full viewport height */
        }

        .reserve-container {
            max-width: 40em;
        }

        .column {
            padding: 10px;
            text-align: left;
            width: 36.4em;
            flex-basis: 50%;
            /* Adjust the width of the columns as needed */

        }
    </style>
</head>

<body>
    <div class="member-info"></div>
    <div class="reserve-container">
        <a class="nav-link" href="../home/home.php#hero">
            <h1 class="text-center" style="font-family: Copperplate; color: whitesmoke;">FLAVA CAFE</h1>
            <span class="sr-only"></span>
        </a>

        <div class="row">
            <div class="column">
                <div id="Search Table">
                    <h2 style=" color:white;">Search for Time</h2>

                    <form id="reservation-form"><br>
                        <div class="form-group">
                            <label for="reservation_date" style="">Select Date</label><br>
                            <input class="form-control" type="date" id="reservation_date" name="reservation_date"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="reservation_time" style="">Available Reservation Times</label>
                            <div id="availability-table">
                                <?php
                                $availableTimes = array();
                                for ($hour = 10; $hour <= 20; $hour++) {
                                    for ($minute = 0; $minute < 60; $minute += 60) {
                                        $time = sprintf('%02d:%02d:00', $hour, $minute);
                                        $availableTimes[] = $time;
                                    }
                                }
                                echo '<select name="reservation_time" id="reservation_time" style="width:10em;" class="form-control" >';
                                echo '<option value="" selected disabled>Select a Time</option>';
                                foreach ($availableTimes as $time) {
                                    echo "<option  value='$time'>$time</option>";
                                }
                                echo '</select>';
                                if (isset($_GET['message'])) {
                                    $message = $_GET['message'];
                                    echo "<p>$message</p>";
                                }
                                ?>
                            </div>
                        </div>

                        <input type="number" id="head_count" name="head_count" value=1 hidden required>
                        <button type="submit" style="background-color: black; color: rgb(234, 234, 234); " class="btn"
                            name="submit">Search</button>
                    </form>
                </div>
            </div>

            <div class="column right-column">
                <div id="insert-reservation-into-table">
                    <h2 style=" color:white;">Make Reservation</h2>
                    <form id="reservation-form2" method="POST" action="insertReservation.php"
                        enctype="multipart/form-data">
                        <br>
                        <div class="form-group">
                            <label for="customer_name" style="">Customer Name</label><br>
                            <input class="form-control" type="text" id="customer_name" name="customer_name" required>
                        </div>


                        <div class="form-group ">
                            <label for="reservation_date" style="">Reservation Date</label><br>
                            <input type="date" id="reservation_date2" name="reservation_date" value="" readonly
                                required>
                            <input type="time" id="reservation_time2" name="reservation_time" value="" readonly
                                required>
                        </div>

                        <div class="form-group">
                            <label for="table_id_reserve" style="">Available Tables</label>
                            <select class="form-control" name="table_id" id="table_id_reserve" style="width:10em;"
                                required>
                                <option value="" selected disabled>Select a Table</option>
                            </select>
                            <input type="number" id="head_count" name="head_count" hidden>
                        </div>

                        <div class="form-group mb-3">
                            <label for="special_request">Special request</label><br>
                            <textarea class="form-control" id="special_request" name="special_request"> </textarea><br>
                        </div>
                        <!-- upload file -->
                        <div class="form-group mb-4">
                            <label for="proof_payment">Upload Proof of Payment</label><br>
                            <input type="file" id="proof_payment" name="proof_payment" accept="image/*, .pdf" required><br>
                            <small>Accepted formats: JPG, PNG, PDF</small>
                            <button type="submit" style="background-color: black; color: rgb(234, 234, 234); "
                                class="btn" type="submit" name="submit">Make Reservation</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script>
        const viewDateInput = document.getElementById("reservation_date");
        const makeDateInput = document.getElementById("reservation_date");

        $('#reservation-form').submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: 'reservePage_service.php',
                type: 'POST',
                data: $('#reservation-form').serializeArray(),
                dataType: 'JSON',
                success: function (response) {
                    // alert('sukses');
                    $('#reservation_date2').val(response.reservation_date);
                    $('#reservation_time2').val(response.reservation_time);
                    $('#head_count').val(response.head_count);

                    var html = '';
                    $.each(response.availableTablesList, function (index, row) {
                        html += '<option value="' + row.table_id + '">For ' + row.capacity + ' people. (Table Id: ' + row.table_id + ')</option>';
                    });
                    $('#table_id_reserve').html(html);
                }                
            })
        })

        $('#reservation-form2').submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: 'reservePage_service.php',
                type: 'POST',
                data: $('#reservation-form2').serializeArray(),
                dataType: 'JSON',
                success: function (response) {
                    console.log($('#reservation-form2').serializeArray());
                }
            })
        })

        $('#reservation-form2').submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: 'insertReservation.php',
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    alert("Reservation successful!");
                },
                error: function () {
                    alert("Reservation failed. Please try again later.");
                }
            });
        });

        viewDateInput.addEventListener("change", function () {
            makeDateInput.value = this.value;
        });
    </script>
</body>
</html>