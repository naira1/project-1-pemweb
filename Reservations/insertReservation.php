<?php
require_once '../config.php';
//session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // Get the values from the form
  $customer_name = $_POST["customer_name"];
  $table_id = intval($_POST["table_id"]);
  $reservation_time = $_POST["reservation_time"];
  $reservation_date = $_POST["reservation_date"];
  $special_request = $_POST["special_request"];

  // Handle file upload
  if (isset($_FILES['proof_payment']) && $_FILES['proof_payment']['error'] === UPLOAD_ERR_OK) {
    $file_name = $_FILES['proof_payment']['name'];
    $file_tmp = $_FILES['proof_payment']['tmp_name'];

    // Define upload directory
    $upload_dir = "uploads/";

    // Check if upload directory exists, create it if not
    if (!is_dir($upload_dir)) {
      mkdir($upload_dir, 0777, true);
    }

    // Move uploaded file to the upload directory
    $move_result = move_uploaded_file($file_tmp, $upload_dir . $file_name);

    if ($move_result) {
      // Build the file path for database storage
      $file_path = mysqli_real_escape_string($link, $upload_dir . $file_name); // Escape the string for SQL
    } else {
      echo "Error uploading file.";
      exit; // Stop further execution
    }
  } else {
    echo "No file uploaded or an error occurred while uploading.";
    exit; // Stop further execution
  }

  $select_query_capacity = "SELECT capacity FROM restaurant_tables WHERE table_id='$table_id';";
  $results_capacity = mysqli_query($link, $select_query_capacity);

  if ($results_capacity) {
    $row = mysqli_fetch_assoc($results_capacity);
    $head_count = $row['capacity'];

    $reservation_id = intval($reservation_time) . intval($reservation_date) . intval($table_id);

    // Prepare the SQL query for insertion
    $insert_query1 = "INSERT INTO Reservations (reservation_id, customer_name, table_id, reservation_time, reservation_date, head_count, special_request, proof_payment) 
                        VALUES ($reservation_id, '$customer_name', $table_id, '$reservation_time', '$reservation_date', $head_count, '$special_request', '$file_path');";
    $insert_query2 = "INSERT INTO Table_Availability (availability_id, table_id, reservation_date, reservation_time, status) 
                        VALUES ($reservation_id, $table_id, '$reservation_date', '$reservation_time', 'no');";
    mysqli_query($link, $insert_query1);
    mysqli_query($link, $insert_query2);

    header("Location: reservePage.php?reservation=success&reservation_id=$reservation_id");
  } else {
    // Handle the case where the query failed
    echo "Error fetching table capacity: " . mysqli_error($link);
  }
}
?>