<?php
require_once '../config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $selectedDate = $_POST["reservation_date"]; // Selected Date
    $head_count = $_POST["head_count"];  // Number of people
    $selectedTime = date("H:i:s", strtotime($_POST["reservation_time"]));

    // Query to get all reservations for the selected date and time
    $reservedQuery = "SELECT * FROM reservations WHERE reservation_date = '$selectedDate' AND reservation_time = '$selectedTime'";
    $reservedResult = mysqli_query($link, $reservedQuery);

    // Initialize an array to store reserved table IDs
    $reservedTableIDs = array();

    // Collect reserved table IDs
    if ($reservedResult) {
        while ($row = mysqli_fetch_assoc($reservedResult)) {
            $reservedTableIDs[] = $row["table_id"];
        }
    } else {
        echo "Query failed: " . mysqli_error($link);
    }

    // Check available tables
    $availableTablesList = [];

    if (!empty($reservedTableIDs)) {
        $reservedTableIDsString = implode(",", $reservedTableIDs);
        $availableTables = "SELECT table_id, capacity FROM restaurant_tables WHERE capacity >= '$head_count' AND table_id NOT IN ($reservedTableIDsString)";
        $availableResult = mysqli_query($link, $availableTables);

        if ($availableResult) {
            while ($row = mysqli_fetch_assoc($availableResult)) {

            }
            // Construct the reservation link with all table IDs
            $reservedTableIDsString = implode(",", $reservedTableIDs);

        } else {
            echo "Available tables query failed: " . mysqli_error($link);
        }
    } else {
    }

    $table_id_list = $reservedTableIDsString ?? 0;
    $reserved_table_ids = explode(',', $table_id_list);
    $select_query_tables = "SELECT * FROM restaurant_tables WHERE capacity >= '$head_count'";
    if (!empty($reserved_table_ids)) {
        $reserved_table_ids_string = implode(',', $reserved_table_ids);
        $select_query_tables .= " AND table_id NOT IN ($reserved_table_ids_string)";
    }
    $result_tables = mysqli_query($link, $select_query_tables);
    $resultCheckTables = mysqli_num_rows($result_tables);
    if ($resultCheckTables > 0) {
        while ($row = mysqli_fetch_assoc($result_tables)) {
            $availableTablesList[] = $row;
        }
    } else {
    }

    return print_r(json_encode(['reservation_date' => $selectedDate, 'head_count' => $head_count, 'reservation_time' => $selectedTime, 'reserved_table_id' => $reservedTableIDsString ?? 0, 'availableTablesList' => $availableTablesList]));
}
